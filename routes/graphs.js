/**
 * Created by akaliaperumal on 2/19/16.
 */
var express = require('express');
var router = express.Router();
var Handlebars = require('hbs');

var appRoot = require('app-root-path');
var fs = require('fs');


var configurationFile = appRoot + '/config/navbarmenu.json';

/* GET home page. */
router.get('/', function(req, res, next) {
    //var configurationFile = appRoot + '/config/navbarmenu.json';
    res.locals = JSON.parse(fs.readFileSync(configurationFile));
    res.render('graph_chartist',{ title: 'Graphs' });
});

router.get('/flot', function(req, res, next) {
    //var configurationFile = appRoot + '/config/navbarmenu.json';
     res.locals = JSON.parse(fs.readFileSync(configurationFile));
    res.render('index',{ title: 'Graphs',page: 'flot' });
});

router.get('/chartist', function(req, res, next) {
    //var configurationFile = appRoot + '/config/navbarmenu.json';
    res.locals = JSON.parse(fs.readFileSync(configurationFile));
    res.render('index',{ title: 'Graphs',page: 'chartist' });
});

router.get('/morris', function(req, res, next) {
    //var configurationFile = appRoot + '/config/navbarmenu.json';
    res.locals = JSON.parse(fs.readFileSync(configurationFile));
    res.render('index',{ title: 'Graphs',page: 'morris' });
});

router.get('/chartjs', function(req, res, next) {
    //var configurationFile = appRoot + '/config/navbarmenu.json';
    res.locals = JSON.parse(fs.readFileSync(configurationFile));
    res.render('index',{ title: 'Graphs',page: 'chartjs' });
});

router.get('/peity', function(req, res, next) {
    //var configurationFile = appRoot + '/config/navbarmenu.json';
    res.locals = JSON.parse(fs.readFileSync(configurationFile));
    res.render('index',{ title: 'Graphs',page: 'peity' });
});

router.get('/rickshaw', function(req, res, next) {
    //var configurationFile = appRoot + '/config/navbarmenu.json';
    res.locals = JSON.parse(fs.readFileSync(configurationFile));
    res.render('index',{ title: 'Graphs',page: 'rickshaw' });
});

router.get('/sparkline', function(req, res, next) {
    //var configurationFile = appRoot + '/config/navbarmenu.json';
    res.locals = JSON.parse(fs.readFileSync(configurationFile));
    res.render('index',{ title: 'Graphs',page: 'sparkline' });
});



module.exports = router;
