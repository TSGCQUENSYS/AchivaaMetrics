/**
 * Created by akaliaperumal on 2/19/16.
 */
var express = require('express');
var router = express.Router();
var Handlebars = require('hbs');
var appRoot = require('app-root-path');
var fs = require('fs');

router.get('/', function(req, res, next) {

    var configurationFile = appRoot + '/config/navbarmenu.json';
    res.locals = JSON.parse(fs.readFileSync(configurationFile));
    res.render('navbar',{ title: 'navbar' });
});


module.exports = router;
