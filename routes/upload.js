var express = require('express');
var router = express.Router();

var appRoot = require('app-root-path');
var fs = require('fs');


/* GET home page. */
/*router.get('/', function(req, res, next) {

  var configurationFile = appRoot + '/config/navbarmenu.json';
  res.locals = JSON.parse(fs.readFileSync(configurationFile));

  res.render('index', { title: 'Achivaa' });


});

*/
router.get('/', function(req, res, next) {
  var configurationFile = appRoot + '/config/navbarmenu.json';
  res.locals = JSON.parse(fs.readFileSync(configurationFile));
  res.render('index',{ title: 'Upload',page: 'upload' });
});

module.exports = router;
