var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var hbs= require('hbs');
var fs = require('fs');
var passport = require('passport');
var appRoot = require('app-root-path');

var busboy = require('connect-busboy'); //middleware for form/file upload
var path = require('path');     //used for file path
var fse = require('fs-extra');       //File System - for file manipulation
var moment = require("moment");

var AWS = require('aws-sdk');
var uuid = require('node-uuid');
var zlib = require('zlib');

AWS.config.region = 'us-west-2';


function getFormatedFileName(pFileName, pType){

  var vType = '';
  if (pType === 'test')
    vType = '_test';


  //var fnParsed = path.parse(pFileName);
  //var newFileName = fnParsed.name + vType + moment().format('DDMMYYYYHHmmss') + fnParsed.ext;

  var fnBaseName = path.basename(pFileName);
  var fnExtName = path.extname(pFileName);

  var fnName = fnBaseName.replace(fnExtName, '');

  var newFileName = fnName + vType + moment().format('DDMMYYYYHHmmss') + fnExtName;

  return newFileName;
}


var routes = require('./routes/index');
var users = require('./routes/users');
var login=require('./routes/login');
var navbar=require('./routes/navbar');
var header=require('./routes/header');
var dashboardarea=require('./routes/dashboardarea');
var graphs=require('./routes/graphs');
var upload=require('./routes/upload');

var app = express();


hbs.registerPartial('navbar', fs.readFileSync(__dirname + '/views/navbar.hbs', 'utf8'));
hbs.registerPartial('header', fs.readFileSync(__dirname + '/views/header.hbs', 'utf8'));
hbs.registerPartial('dashboardarea', fs.readFileSync(__dirname + '/views/dashboardarea.hbs', 'utf8'));
hbs.registerPartial('footer', fs.readFileSync(__dirname + '/views/footer.hbs', 'utf8'));
hbs.registerPartial('rightsideMenu', fs.readFileSync(__dirname + '/views/rightsideMenu.hbs', 'utf8'));
hbs.registerPartial('chat', fs.readFileSync(__dirname + '/views/chat.hbs', 'utf8'));

hbs.registerPartial('graph-flot', fs.readFileSync(__dirname + '/views/graph_flot.hbs', 'utf8'));
hbs.registerPartial('graph-chartist', fs.readFileSync(__dirname + '/views/graph_chartist.hbs', 'utf8'));
hbs.registerPartial('graph-morris', fs.readFileSync(__dirname + '/views/graph_morris.hbs', 'utf8'));


hbs.registerPartials(__dirname + '/views/partials');
// view engine setup

hbs.registerHelper('ifCond', function(v1, v2, options) {
  if(v1 === v2) {
    return options.fn(this);
  }
  return options.inverse(this);
});

app.set('views', path.join(__dirname, 'views'));

app.set('view engine', 'hbs');

//app.use(express.static(__dirname + '/public'));

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

//app.use(require('less-middleware')(path.join(__dirname, 'images')));

app.use(express.static(path.join(__dirname, 'public')));

app.use('/graphs', express.static(__dirname + '/public'));
app.use('/js', express.static(__dirname + '/public/javascripts'));
app.use('/css', express.static(__dirname + '/public/stylesheets'));


app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(busboy());



app.use(passport.initialize());
app.use(passport.session());


app.use('/', login);

app.use('/login', login);

app.use('/navbar', navbar);

app.use('/index', routes);

app.use('/users', users);

app.use('/header', header);

app.use('/graphs', graphs);

app.use('/dashboardarea',dashboardarea);

app.use('/upload',upload);

app.route('/api/upload')
    .post(function (req, res, next) {
      console.log('Processing test request.');

      var fstream;

      req.busboy.on('file', function (fieldname, file, filename) {
        console.log("Uploading: " + filename);

        var formatedFileName = getFormatedFileName(filename, 'test');

        try{
          //fstream = fse.createOutputStream(config.testPath + req.cookies.sftpuser + '/' + formatedFileName + '.test.part');

          fstream = fse.createOutputStream(__dirname+'/uploads/' + formatedFileName + '.test.part');

          //Path where image will be uploaded
          file.pipe(fstream);
          //log.info(config.testPath + req.cookies.sftpuser + '/' + formatedFileName);

          fstream.on('close', function () {
            console.log("Copy from temp to live started")
            //fs.rename(config.testPath + req.cookies.sftpuser + '/' + formatedFileName + '.test.part', config.testPath + req.cookies.sftpuser + '/' + formatedFileName, function(err) {

            // Load the stream


            //var body = fs.createReadStream(__dirname+'/uploads/' + formatedFileName + '.test.part').pipe(zlib.createGzip());
            var body = fs.createReadStream(__dirname+'/uploads/' + formatedFileName + '.test.part');

            var uuid = require('node-uuid');
            AWS.config.update({accessKeyId: '', secretAccessKey: ''});
            AWS.config.update({region: 'us-east-1'});
            //AWS.config.region = 'us-east-1';

// Upload the stream
            var s3obj = new AWS.S3({params: {Bucket: 'achivaametricsbucket', Key: formatedFileName}});
            s3obj.upload({Body: body}, function(err, data) {
              if (err) console.log("An error occurred", err);
              console.log("Uploaded the file at", data.Location);
            });



            // Load the SDK and UUID
            //var AWS = require('aws-sdk');


// Create an S3 client
            //var s3 = new AWS.S3();

// Create a bucket and upload something into it
         //   var bucketName = 'node-sdk-sample-' + uuid.v4();
          //  var keyName = 'hello_world.txt';

           /* s3.createBucket({Bucket: bucketName}, function() {
              var params = {Bucket: bucketName, Key: keyName, Body: 'Hello World!'};
              s3.putObject(params, function(err, data) {
                if (err)
                  console.log(err)
                else
                  console.log("Successfully uploaded data to " + bucketName + "/" + keyName);
              });
            });
*/




            fs.rename(__dirname+'/uploads/' +formatedFileName+'.test.part', __dirname+'/uploads/' + formatedFileName, function(err) {
              if ( err ) {
                console.log('ERROR: ' + err);
            //    log.info('ERROR: ' + err);
                res.status(500).send('Temp to test copy error ')
              }
              else{
                console.log("Copy from temp to live complete")
            //    res.sendStatus(200);
              }
            });
            console.log("Upload Finished of " + filename);
            res.redirect('/upload');

          });

          fstream.on('error', function (e) {
            //log.info(e);
            console.log(e);
            file.read();
            res.status(500).send('File could not be copied to the server, most likely the landing directory could not be found.');
          });

        }
        catch(err){
          //log.info(err);
          //log.info('Error with the file stream module')
          res.status(500).send('Error with the file stream module')
        }


      });

      req.busboy.on('error', function (e) {
        //log.info(e);
        console.log(e);
      });

      req.pipe(req.busboy);
    });


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
